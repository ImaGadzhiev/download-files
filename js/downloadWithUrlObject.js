(function () {
  const button = document.getElementById("download_with_url_object");
  const content = ["content-1", "content-2", "content-3"];
  
  const createLink = () => {
    let link = document.createElement('a');
    link.download = 'hello.txt';
    return link;
  }
  
  const generateBlob = () => {
    for (const [index, value] of content.entries()) {
      const blob = new Blob([value], { type: "text/plain" });
      download(blob, index);
    }
  }
  
  const download = (blob, index) => {
    const link = createLink();
    link.href = URL.createObjectURL(blob);
    link.download = `content-${index+1}.txt`;
    link.click();
    URL.revokeObjectURL(link.href);
  }
  
  button.addEventListener("click", generateBlob);
}) ();
