(function () {
  const button = document.getElementById("download_with_reader");
  const content = ["content-1", "content-2", "content-3"];
  
  const createLink = () => {
    let link = document.createElement('a');
    link.download = 'hello.txt';
    return link;
  }
  
  const generateBlob = () => {
    for (const [index, value] of content.entries()) {
      const blob = new Blob([value], { type: "text/plain" });
      download(blob, index);
    }
  }

  const download = (blob, index) => {
    const link = createLink();
    
    let reader = new FileReader();
    reader.readAsDataURL(blob);
    
    reader.onload = function () {
      link.href = reader.result;
      link.download = `content-${index+1}.txt`;
      link.click();
    }
  }
  
  button.addEventListener("click", generateBlob);
}) ();
