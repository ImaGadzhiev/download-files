(function () {
  const button = document.getElementById("download_with_request");
  const urls = ["images/image-1.jpg", "images/image-2.jpg", "images/image-3.jpg"];
  
  const delay = () => new Promise(resolve => setTimeout(resolve, 1000));
  
  const downloadWithRequest = async () => {
    for await (const [index, url] of urls.entries()) {
      await delay();
      const link = document.createElement("a");
      link.href = url;
      link.download = `image-${index+1}`;
      link.click();
    }
  }
  
  button.addEventListener("click", downloadWithRequest);
}) ();
